
# this is the magic interpreted by Sonata, referring to construct_tab below:

### BEGIN PLUGIN INFO
# [plugin]
# plugin_format: 0, 0
# name: Local MPD
# version: 0, 0, 1
# description: A tab for controlling local MPD.
# author: Tuukka Hastrup
# author_email: Tuukka.Hastrup@iki.fi
# url: http://sonata.berlios.de
# license: GPL v3 or later
# [capabilities]
# tabs: construct_tab
### END PLUGIN INFO

import subprocess, locale
from pwd import getpwuid

import gobject, gtk

from sonata.misc import escape_html

import ConfigParser
import os


class ownconf() :
    """ read write and ask some var to configure the plugin
    """
    def __init__(self):
        self.save_file = os.path.expanduser('~/.config/sonata/plugin_localmpd')
        self.config = ConfigParser.RawConfigParser()

        edit = None
        restart = None
        refresh = None
        try :
            self.config.read(self.save_file)
            edit = self.config.get("general", 'edit')
            restart = self.config.get("general", 'restart')
            refresh = self.config.getint("general", 'refresh')
        except :
            pass

        self.edit =["gksu", "gedit", "/etc/mpd.conf"]
        self.restart=["gksu", "service", "mpd", "restart"]
        self.refresh=1000

        if edit :
            self.edit = edit.split(' ')
        if restart :
            self.restart = restart.split(' ')
        if refresh :
            self.refresh = refresh



    def set_conf(self) :
        vbox = gtk.VBox(spacing=2)

        hbox1 = gtk.HBox()
        edit = gtk.Entry()
        edit.set_text(' '.join(self.edit))
        edit.set_editable(True)
        label1 = gtk.Label()
        label1.set_markup('edit mpd')
        hbox1.add(label1)
        hbox1.add(edit)

        hbox2 = gtk.HBox()
        restart = gtk.Entry()
        restart.set_text(' '.join(self.restart))
        restart.set_editable(True)
        label2 = gtk.Label()
        label2.set_markup('restart')
        hbox2.add(label2)
        hbox2.add(restart)


        hbox3 = gtk.HBox()
        adjust = gtk.Adjustment(value=self.refresh, lower=500, upper=90000, step_incr=10, page_incr=1, page_size=0)
        refresh = gtk.SpinButton(adjustment=adjust, climb_rate=0.0, digits=0)
        label3 = gtk.Label()
        label3.set_markup('refresh')
        hbox3.add(label3)
        hbox3.add(refresh)



        vbox.pack_start(hbox1, False, False)
        vbox.pack_start(hbox2, False, False)
        vbox.pack_start(hbox3, False, False)

        confbutton = gtk.Button("ok")
        confbutton.connect('clicked', self.insert_conf, edit, restart, refresh)

        vbox.pack_start(confbutton, False, False)

        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_title("localmpd")
        window.add(vbox)
        window.show_all()


    def insert_conf(self, widget, edit, restart, refresh):
        self.edit = edit.get_text().split(' ')
        self.restart = restart.get_text().split(' ')
        self.refresh = refresh.get_value_as_int()

        if not self.config.has_section("general") :
            self.config.add_section("general")

        self.config.set("general", 'edit', ' '.join(self.edit))
        self.config.set("general", 'restart', ' '.join(self.restart))
        self.config.set("general", 'refresh', self.refresh)


        with open(self.save_file, 'wb') as conf :
            self.config.write(conf)

        


localmpdconf=ownconf()

  
class Netstat(object):
    TCP_STATE_NAMES = ("ESTABLISHED SYN_SENT SYN_RECV FIN_WAIT1 FIN_WAIT2 "
               "TIME_WAIT CLOSE CLOSE_WAIT LAST_ACK LISTEN CLOSING"
               .split())
    def __init__(self):
        self.connections = None

    def _addr(self, part):
        host, port = part.split(':')
        port = str(int(port, 16))
        if len(host) == 8:
            parts = [host[0:2], host[2:4], host[4:6], host[6:8]]
            parts = [str(int(x, 16)) for x in parts]
            host = '.'.join(reversed(parts))
        else:
            host = "IPV6" # FIXME
        if host == '0.0.0.0':
            host = '*'
        elif host == '127.0.0.1':
            host = 'localhost'
        if port == '0':
            port = '*'
        return (host, port)

    def read_connections(self):
        def fromhex(x):
            return int(x, 16)
        self.connections = []
        for name in '/proc/net/tcp', '/proc/net/tcp6':
            f = open(name,'rt')
            headings = f.readline()
            for line in f:
                parts = line.split()
                if len(parts) < 10:
                    continue # broken line
                local = self._addr(parts[1])
                remote = self._addr(parts[2])
                state = self.TCP_STATE_NAMES[
                    fromhex(parts[3])-1]
                queueparts = parts[4].split(':')
                queues = tuple(map(fromhex,queueparts))
                uid, _timeout, inode = map(int, parts[7:10])
                if len(parts[1].split(":")[0]) == 8:
                    proto = "tcp"
                else:
                    proto = "tcp6"
                self.connections += [(proto, local, remote, state, queues, uid, inode)]

    def format_connections(self):
        t = "%-5s %6s %6s %15s:%-5s %15s:%-5s  %-11s  %s"
        headings = "Proto Send-Q Recv-Q Local Port Remote Port State User".split()
        return (t % tuple(headings) + '\n' +
            '\n'.join([t % (proto, rxq, txq, localh, localp, remoteh, remotep, state, getpwuid(uid)[0])
                   for proto, (localh, localp), (remoteh, remotep), state, (txq, rxq), uid, inode in self.connections
                   if localp == '6600' or remotep == '6600'
                   or getpwuid(uid)[0] == 'mpd']))



 


def update(label):
    # schedule next update
    gobject.timeout_add(localmpdconf.refresh, update, label)

    # don't update if not visible
    if not label.window or not label.window.is_viewable():
        return

    netstat = Netstat()
    netstat.read_connections()
    netstats = netstat.format_connections()

    # XXX replace the shell commands with python code
    commands = [("Processes", "ps wwu -C mpd".split()),
            ("Files", ["sh", "-c", "ls -ldh /etc/mpd.conf /var/lib/mpd /var/lib/mpd/* /var/lib/mpd/*/*"]),
            ]
    outputs = [(title, subprocess.Popen(command,
                        stdout=subprocess.PIPE,
                        stderr=subprocess.PIPE
                        ).communicate())
           for title, command in commands]

    sections = [outputs[0], ("Networking", (netstats, "")), outputs[1]]
    text = '\n'.join(["<b>%s</b>\n<tt>%s</tt><i>%s</i>\n" %
              (title, escape_html(stdout), escape_html(stderr))
              for title, (stdout, stderr) in sections])
    label.set_markup(text.decode(locale.getpreferredencoding(),
                     'replace'))



        




# nothing magical here, this constructs the parts of the tab when called:
def construct_tab():
    vbox = gtk.VBox(spacing=2)
    vbox.props.border_width = 2
    buttonbox = gtk.HBox(spacing=2)
    editbutton = gtk.Button("Edit /etc/mpd.conf")
    editbutton.connect('clicked', lambda *args:subprocess.Popen(
            localmpdconf.edit))
    buttonbox.pack_start(editbutton, False, False)
    restartbutton = gtk.Button("Restart the mpd service")
    restartbutton.connect('clicked', lambda *args:subprocess.Popen(
            localmpdconf.restart))
    buttonbox.pack_start(restartbutton, False, False)

    confbutton = gtk.Button("Configure command")
    confbutton.connect('clicked', lambda *args:localmpdconf.set_conf())
    buttonbox.pack_start(confbutton, False, False)

    vbox.pack_start(buttonbox, False, False)
    label = gtk.Label()
    label.set_properties(xalign=0.0, xpad=5, yalign=0.0, ypad=5,
                 selectable=True)
    vbox.pack_start(label, False, False)

    update(label)

    window = gtk.ScrolledWindow()
    window.set_properties(hscrollbar_policy=gtk.POLICY_AUTOMATIC,
                  vscrollbar_policy=gtk.POLICY_AUTOMATIC)
    window.add_with_viewport(vbox)
    window.show_all()

    # (tab content, icon name, tab name, the widget to focus on tab switch)
    return (window, None, "Local MPD", None)
