from warnings import warn

import dbus
import avahi

import library
import config


class Zeroconf:
    def __init__(self, TYPE='_mpd._tcp'):
        self.conf = config.Config(_('Default Profile'),
                '%s %%A %s %%B' % (_('by'), _('from'),),
                library.library_set_data)
        self.conf.settings_load_real(library.library_set_data)
        dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
        bus = dbus.SystemBus()
        try:
            self.server = dbus.Interface(bus.get_object(avahi.DBUS_NAME, '/'),
                    'org.freedesktop.Avahi.Server')
        except dbus.exceptions.DBusException:
            warn("Avahi disabled.")
            return
        sbrowser = dbus.Interface(bus.get_object(avahi.DBUS_NAME,
                self.server.ServiceBrowserNew(avahi.IF_UNSPEC,
                    avahi.PROTO_UNSPEC, TYPE, 'local', dbus.UInt32(0))),
                avahi.DBUS_INTERFACE_SERVICE_BROWSER)
        sbrowser.connect_to_signal("ItemNew", self.ItemNew_cb)

    def ItemNew_cb(self, interface, protocol, name, stype, domain, flags):
        self.server.ResolveService(interface, protocol, name, stype,
            domain, avahi.PROTO_UNSPEC, dbus.UInt32(0),
            reply_handler=self.service_resolved,
            error_handler=self.print_error)

    def service_resolved(self, *args):
        """If a discovered mpd service's name is not currently in the list of
        profile names, add its configuration information as a new profile."""

        if ':' in args[7]:
            return  # Filters out IPv6.
        if args[2] not in self.conf.profile_names:
            print 'New Service Resolved: %s@%s' % (args[2], args[5])
            self.conf.profile_names.append(str(args[2]))
            self.conf.host.append(str(args[5]))
            self.conf.port.append(str(args[8]))
            self.conf.password.append('')
            self.conf.musicdir.append('')
            self.conf.settings_save_real(library.library_set_data)

    def print_error(self, *args):
        print 'error_handler'
        print args[0]
