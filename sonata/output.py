"""This module implements the format strings used to display info.

Example usage :

from output import aprint

aprint(2, 'print this')
"""

import sys

class message():
    """show output messages or write it like user's preference
    0=>'mute', 1=>'error', 2=>'warn', 3=>'info'
    please use reinit(level, isonterm, outfile) before the first use
    """
    level = 2
    onterm = True
    outfile = False
      
    
    def __init__(self) :
        self.equiv = [ 'mute', 'error', 'warn', 'info' ]

    def reinit(self, level=3, onterm=True, outfile=False) :

        message.level = level
        message.outfile  = outfile
        message.onterm  = onterm

    def infile(self, toprint) :
        try :
            f = open(message.outfile, 'a')
            f.write("sonata : %s\n" % (self.lvl() + ' ' + toprint))
            f.close()
        except :
            sys.stderr.write('failed write in log file\n')
            sys.exit(1)
    
    def aprint(self, level, toprint ) :
        if (level <= message.level) :
            if message.onterm :
                print (toprint)
            if message.outfile :
                self.infile(toprint)

    def error(self, toprint) :
        if message.onterm :
            sys.stderr.write(toprint)
        if message.outfile :
            self.infile(toprint)
            

    def lvl(self) :
        return self.equiv[message.level]

    def isatty(self):
        import os
        return os.isatty(sys.stdin.fileno())


out = message()

def aprint(lvl, msg) :
    out.aprint(lvl, msg)
def aerror( msg) :
    out.error( msg)
def reinit(a, b, c) :
    out.reinit(a, b, c)
def level() :
    return out.lvl()
def isatty() :
    return out.isatty()


