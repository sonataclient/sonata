"""
This module contains various constant definitions that any other
module can import without risk of cyclic imports.

Most of the constants are enum-like, that is, they provide symbolic
names for a set of values.

XXX Should some of these be moved to be private in some module, or
into config?

Example usage:
from consts import consts
...
if view == consts('VIEW_ALBUM'): ...
"""
import ConfigParser
import os
import sys

from output import aerror

class Constants:
    """This class contains the constant definitions as attributes."""
    def __init__(self) :
        self.config = ConfigParser.RawConfigParser()
        self.conffile = os.path.join(os.path.dirname(__file__), 'data', 'Constants.cfg')
        self.config.read(self.conffile) 

    def __call__(self, const, islist=False) :
        if not islist :
            try :
                value = self.config.getint('int', const)
            except :
                value = None
        elif islist :
            try :
                value = self.config.get('list', const).split(',')
            except :
                value = None
        else :
            value = None


        if (value == None) :
            import sys
            aerror('variable not found %s' % const )
            sys.exit(1)
        return value

