#!/usr/bin/env python
"""Sonata is a simple GTK+ client for the Music Player Daemon."""

import sys
import platform
import locale
import gettext
import os


# XXX insert the correct sonata package dir in sys.path

major, minor1, minor2, s, tmp = sys.version_info
if major == 2 and minor1 < 7 or major < 2:
    raise SystemExit("Sonata requires Python 2.7")

try:
    import sonata
except ImportError:
    sys.stderr.write('Python failed to find the sonata modules.\n')
    sys.stderr.write('\nSearched in the following directories:\n' +
             '\n'.join(sys.path) + '\n')
    sys.stderr.write('\nPerhaps Sonata is improperly installed?\n')
    sys.exit(1)

try:
    import sonata.version
    import sonata.output as out
except ImportError:
    sys.stderr.write('Python failed to find the sonata modules.\n'
            '\nAn old or incomplete installation was '
            'found in the following directory:\n%s\n'
            '\nPerhaps you want to delete it?\n' %\
             (os.path.dirname(sonata.__file__),))
    sys.exit(1)



# XXX check that version.VERSION is what this script was installed for


## Apply global fixes:

# the following line is to fix python-zsi 2.0 and thus lyrics in ubuntu:
# https://bugs.launchpad.net/ubuntu/+source/zsi/+bug/208855
sys.path.append('/usr/lib/python2.5/site-packages/oldxml')

# hint for gnome.init to set the process name to 'sonata'
if platform.system() == 'Linux':
    sys.argv[0] = 'sonata'

# apply as well:
    try:
        import ctypes
        libc = ctypes.CDLL('libc.so.6')
        PR_SET_NAME = 15
        libc.prctl(PR_SET_NAME, sys.argv[0], 0, 0, 0)
    # if it fails, it fails
    except Exception:
        pass


## Apply locale and translation:

from sonata import misc
misc.setlocale()

# let gettext install _ as a built-in for all modules to see
# XXX what's the correct way to find the localization?
try:
    gettext.install('sonata', os.path.join(sonata.__file__.split('/lib')[0],
        'share', 'locale'), unicode=1)
except:
    out.aprint(2, 'Warning: trying to use an old translation')
    gettext.install('sonata', '/usr/share/locale', unicode=1)
gettext.textdomain('sonata')


## Check initial dependencies:

# Test python version:
if sys.version_info < (2, 5):
    out.aerror('Sonata requires Python 2.5 or newer. Aborting...\n')
    sys.exit(1)

try:
    import mpd
except:
    out.aerror('Sonata requires python-mpd. Aborting...\n')
    sys.exit(1)


## Initialize the plugin system:

from sonata.pluginsystem import pluginsystem
pluginsystem.find_plugins()
pluginsystem.notify_of('enablables',
               lambda plugin, cb: cb(True),
               lambda plugin, cb: cb(False))


## Load the command line interface:

from sonata import cli
args = cli.Args()
args.parse(sys.argv)

## Deal with GTK:

if not args.skip_gui:
    # importing gtk does sys.setdefaultencoding('utf-8'), sets locale etc.
    import gtk
    if gtk.pygtk_version < (2, 12, 0):
        out.aerror('Sonata requires PyGTK 2.12.0 or newer.'
                'Aborting...\n')
        sys.exit(1)
    # fix locale
    misc.setlocale()
else:
    class FakeModule(object):
        pass
    # make sure the ui modules aren't imported
    for m in ('gtk', 'pango', 'sonata.ui', 'sonata.breadcrumbs',):
        if m in sys.modules:
            out.aprint(2, 'Warning: module %s imported in CLI mode' % m)
        else:
            sys.modules[m] = FakeModule()
    # like gtk, set utf-8 encoding of str objects
    # hack access to setdefaultencoding
    reload(sys)
    sys.setdefaultencoding('utf-8')


## Global init:

from socket import setdefaulttimeout as socketsettimeout
socketsettimeout(5)

if not args.skip_gui:
    gtk.gdk.threads_init()

    # we don't use gtk.LinkButton, but gtk.AboutDialog does;
    # in gtk 2.16.0 without this, the about uri opens doubly:
    gtk.link_button_set_uri_hook(lambda *args: None)



## CLI actions:

args.execute_cmds()


## Load the main application:

from sonata import main

app = main.Base(args)
try:
    app.main()
except KeyboardInterrupt:
    pass
